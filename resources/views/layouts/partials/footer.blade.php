{{--<div class="">--}}
    <div class="chart">
        <canvas id="chart-bar" class="chart-canvas"></canvas>
    </div>
<footer class="footer py-4  ">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">
                <div class="copyright text-center text-sm text-muted text-lg-start">
                    ©
                    <script data-cfasync="false"
                            src="{{asset('assets/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    ,
                    made with <i class="fa fa-heart"></i> by
                    <a href="https://www.creative-tim.com/" class="font-weight-bold" target="_blank">Cyntrek</a>
                    for a better web.
                </div>
            </div>

        </div>
    </div>
</footer>

