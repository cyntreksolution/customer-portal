<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
<!-- Nucleo Icons -->
<link href="{{asset('assets/css/nucleo-icons.css')}}'" rel="stylesheet" />
<link href="{{asset('assets/css/nucleo-svg.css')}}" rel="stylesheet" />
<link href="{{asset('assets/tostr/toastr.min.css')}}" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="{{asset('assets/kit.fontawesome.com/42d5adcbca.js')}}" crossorigin="anonymous"></script>
<!-- Material Icons -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
<!-- CSS Files -->
<link id="pagestyle" href="{{asset('assets/css/material-dashboard.min3294.css?v=3.0.1')}}" rel="stylesheet" />
<!-- Anti-flicker snippet (recommended)  -->
<style>
    .async-hide {
        opacity: 0 !important
    }
</style>
