@extends('layouts.master')
@section('content')
    <div class="container-fluid my-3 py-3">
        <div class="row mb-5">

            <div class="col-lg-12 mt-lg-0 mt-4">
                <!-- Card Profile -->

                <!-- Card Basic Info -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="mb-0">Sales Return Table</h5>
                                <p class="text-sm mb-0">
                                    View all the Sales from the previous year.
                                </p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-flush" id="datatable-search">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Invoice No</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Agent</th>
                                        <th>Tel</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($salesReturns->data as $order)
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">

                                                    <p class="text-xs font-weight-normal ms-2 mb-0">#{{$order->invoice_number}}</p>
                                                </div>
                                            </td>
                                            <td class="font-weight-normal">
                                                <span class="my-2 text-xs">{{$order->ordered_time}}</span>
                                            </td>

                                            <td class="text-xs font-weight-normal">
                                                <span class="my-2 text-xs">Rs. {{$order->amount}}</span>
                                            </td>
                                            <td class="text-xs font-weight-normal">
                                                <span class="my-2 text-xs">{{$order->agent->name_with_initials}}</span>
                                            </td>
                                            <td class="text-xs font-weight-normal">
                                                <span class="my-2 text-xs">{{$order->agent->telephone1}}</span>
                                            </td>
                                            <td class="text-xs font-weight-normal">
                                                <a class="mr-2 ml-2 text-warning fa-2x" href="{{url('sales-return/view/'.$order->id)}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>






                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-end">

                                        @for ($i = 1; $i < $salesReturns->page->page_count; $i++)
                                            @if($i ==1)
                                                <li class="page-item ">
                                                    <a class="page-link" href="{{url('/sales-return/index?pageId='.($salesReturns->page->current-1))}}" tabindex="-1">
                                                <span class="material-icons">
                                                  keyboard_arrow_left
                                                </span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                            @endif

                                            <li class="page-item" @if($i == $salesReturns->page->current ? "active" : null) @endif><a class="page-link" href="{{url('/sales-return/index?pageId='.$i)}}">{{$i}}</a></li>

                                            @if($salesReturns->page->page_count ==$i)
                                                <li class="page-item">
                                                    <a class="page-link" href="{{url('/sales-return/index?pageId='.($salesReturns->page->current+1))}}">
                                                <span class="material-icons">
                                                  keyboard_arrow_right
                                                </span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endfor


                                    </ul>
                                </nav>






                            </div>
                        </div>
                    </div>
                </div>
                <!-- Card Change Password -->


                <!-- Card Delete Account -->

            </div>
        </div>

    </div>
@endsection
