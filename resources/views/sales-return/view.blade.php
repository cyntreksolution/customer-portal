@extends('layouts.master')
@section('content')
    <div class="container-fluid my-3 py-3">
        <div class="row mb-5">

            <div class="col-lg-12 mt-lg-0 mt-4">
                <!-- Card Profile -->

                <!-- Card Basic Info -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="mb-0">Return Items Table</h5>
                                <p class="text-sm mb-0">
                                    View all the Sales from the previous year.
                                </p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-flush" id="datatable-search">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Qty</th>
                                        <th>Amount</th>
                                        <th>Total</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($returnItems->data as $items)
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">

                                                    <p class="text-xs font-weight-normal ms-2 mb-0">#{{$items->id}}</p>
                                                </div>
                                            </td>
                                            <td class="font-weight-normal">
                                                <span class="my-2 text-xs">{{isset($items->product) ?$items->product->name : null}}</span>
                                            </td>

                                            <td class="text-xs font-weight-normal">
                                                <span class="my-2 text-xs">Rs. {{$items->qty}}</span>
                                            </td>
                                            <td class="text-xs font-weight-normal">
                                                <span class="my-2 text-xs">Rs. {{$items->unit_price}}</span>
                                            </td>
                                            <td class="text-xs font-weight-normal">
                                                <span class="my-2 text-xs">Rs. {{($items->qty * $items->unit_price)}}</span>
                                            </td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>






                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-end">

                                        @for ($i = 1; $i < $returnItems->page->page_count; $i++)
                                            @if($i ==1)
                                                <li class="page-item ">
                                                    <a class="page-link" href="{{url('/api/order/get-sales-return/items'.$id.'?pageId='.($returnItems->page->current-1))}}" tabindex="-1">
                                                <span class="material-icons">
                                                  keyboard_arrow_left
                                                </span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                            @endif

                                            <li class="page-item" @if($i == $returnItems->page->current ? "active" : null) @endif><a class="page-link" href="{{url('/api/order/get-sales-return/items'.$id.'?pageId='.$i)}}">{{$i}}</a></li>

                                            @if($returnItems->page->page_count ==$i)
                                                <li class="page-item">
                                                    <a class="page-link" href="{{url('/api/order/get-sales-return/items'.$id.'?pageId='.($returnItems->page->current+1))}}">
                                                <span class="material-icons">
                                                  keyboard_arrow_right
                                                </span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endfor


                                    </ul>
                                </nav>






                            </div>
                        </div>
                    </div>
                </div>
                <!-- Card Change Password -->


                <!-- Card Delete Account -->

            </div>
        </div>

    </div>
@endsection
