@extends('layouts.master')
@section('content')
    <div class="container-fluid mt-4">
        <div class="row align-items-center">
            <div class="col-lg-4 col-sm-8">
                <div class="nav-wrapper position-relative end-0">

                    <h6 class="font-weight-bolder">Create Shop</h6>

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid my-3 py-3">
        <div class="row mb-5">

            <div class="col-lg-12 mt-lg-0 mt-4">
                <!-- Card Profile -->

                <!-- Card Basic Info -->
                <div class="card mt-4 m-1" id="basic-info">
                    <div class="card-header">
                        <h5>Shop Info</h5>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>Name</label>
                                    <input type="text" class="form-control" placeholder="Alec">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>Phone Number</label>
                                    <input type="number" class="form-control" placeholder="+40 735 631 620">
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="example@email.com">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>Address Line 1</label>
                                    <input type="text" class="form-control" placeholder="Sydney, A">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>Address Line 2</label>
                                    <input type="text" class="form-control" placeholder="Sydney, A">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>City</label>
                                    <input type="text" class="form-control" placeholder="Sydney, A">
                                </div>
                            </div>
                        </div>

                        <div class="d-flex align-items-center mb-sm-0 mb-4 m-3">
                            <div class="w-100 text-end">
                                <button class="btn btn-outline-secondary mb-3 mb-md-0 ms-auto" type="button" name="button">Save</button>
                                <button class="btn bg-gradient-danger mb-0 ms-2" type="button" name="button">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Card Change Password -->
                <div class="card mt-4 m-1" >
                    <div class="card-header">
                        <h5 class="mb-0">Sales Table</h5>
                        <p class="text-sm mb-0">
                            View all the Sales from the previous year.
                        </p>
                        <div class="w-100 text-end">
                            <button class="btn bg-gradient-success mb-0 ms-2" type="button" name="button" data-bs-toggle="modal" data-bs-target="#exampleModalSignUp">New Member</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-flush" id="datatable-search">
                            <thead class="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Birth Date</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                                <td class="text-xs font-weight-normal">
                                    <div class="d-flex align-items-center">
                                        <img src="../../../assets/img/team-2.jpg" class="avatar avatar-xs me-2" alt="user image">
                                        <span>Orlando Imieto</span>
                                    </div>
                                </td>

                                <td class="text-xs font-weight-normal">
                                    <span class="my-2 text-xs">+9477-897-7856</span>
                                </td>
                                <td class="text-xs font-weight-normal">
                                    <span class="my-2 text-xs">1995-09-27</span>
                                </td>
                            </tr>
                            <tr>

                                <td class="text-xs font-weight-normal">
                                    <div class="d-flex align-items-center">
                                        <img src="../../../assets/img/team-1.jpg" class="avatar avatar-xs me-2" alt="user image">
                                        <span>Alice Murinho</span>
                                    </div>
                                </td>


                                <td class="text-xs font-weight-normal">
                                    <span class="my-2 text-xs">+9477-897-7856</span>
                                </td>
                                <td class="text-xs font-weight-normal">
                                    <span class="my-2 text-xs">1995-09-27</span>
                                </td>
                            </tr>
                            <tr>

                                <td class="text-xs font-weight-normal">
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-xs me-2 bg-gradient-dark">
                                            <span>M</span>
                                        </div>
                                        <span>Michael Mirra</span>
                                    </div>
                                </td>

                                <td class="text-xs font-weight-normal">
                                    <span class="my-2 text-xs">+9477-897-7856</span>
                                </td>
                                <td class="text-xs font-weight-normal">
                                    <span class="my-2 text-xs">1995-09-27</span>
                                </td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>


                <!-- Card Delete Account -->

            </div>
        </div>

    </div>

    <div class="modal fade" id="exampleModalSignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalSignTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="card card-plain">
                        <div class="card-header pb-0 text-left">
                            <h5 class="">Add Member</h5>
                            <!--              <p class="mb-0">Enter your email and password to register</p>-->
                        </div>
                        <div class="card-body pb-3">
                            <form role="form text-left">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Name</label>
                                    <input type="text"  class="form-control">
                                </div>
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Contact Number</label>
                                    <input type="text"  class="form-control">
                                </div>
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label">Birth Date</label>
                                    <input type="date"  class="form-control">
                                </div>

                                <div class="text-center">
                                    <button type="button" class="btn bg-gradient-primary btn-lg btn-rounded w-100 mt-4 mb-0">Save</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
