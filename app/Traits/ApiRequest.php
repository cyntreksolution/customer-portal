<?php


namespace App\Traits;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;

trait ApiRequest
{
    private $token;
    private $client;


    public function __construct()
    {
        try {
            $this->client = new Client();
            $this->token = "Bearer " . $this->getTokenFromSession();
        }catch (\Exception $exception){

        }

    }


    private function getPersonalAccessToken($usernames, $password)
    {

        $endpoint = env('API_URL') . "/oauth/token";
        $username = $usernames;
        $userPassword = $password;
        $clientId = 6;
        $clientSecret = 'fO167Ii2o1lMrTjqO4tfNejhv3DBBObIkM2GRiKc';

        try {
            $response = $this->client->request('POST', $endpoint, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $clientId,
                    'client_secret' => $clientSecret,
                    'username' => $username,
                    'password' => $userPassword,
                ]]);

            $statusCode = $response->getStatusCode();
            $content = $response->getBody();
            $token = json_decode($content)->access_token;
            $this->putTokenToSession($token);
            $this->getUser();
            return $token;
        } catch (\Exception $e){
            if($e->getCode() == 400){
                $token =null;
                return $token;
            }
        }

    }

    public function getUser()
    {
        $token = $this->getTokenFromSession();

        $endpoint = env('API_URL') . "/api/user";

        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer " . $token
            ],
        ]);
        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
            session()->put('userId', $data->id);
            session()->put('email', $data->email);
            session()->put('shopId', $data->shop_id);
        }
        return $data;
    }

    private function userclear()
    {
        session()->forget('userId');
        session()->forget('email');
        session()->forget('shopId');
    }

    private function putTokenToSession($token)
    {
        session()->put('api_token', $token);
    }

    private function getTokenFromSession()
    {
        if ($this->isSessionExists()) {
            return session()->get('api_token');
        } else {
            return redirect(route('login.index'));
        }
    }

    private function isSessionExists()
    {
        return Session::has('api_token');
    }

    public function sales($pageNumber=null)
    {
        $token = $this->getTokenFromSession();
        $shopId = session()->get('shopId');
        $endpoint = env('API_URL') . "/api/order/get-orders";

        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer " . $token
            ],
            'query' => ['shop_id'=>$shopId,
                'per_page' => 2,
                 'page_number'=> $pageNumber]
        ]);
        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function salesView($pageNumber=null,$id)
    {
        $token = $this->getTokenFromSession();

        $endpoint = env('API_URL') . "/api/order/get-orders/items";

        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer " . $token
            ],
            'query' => ['order_id'=>$id,
                'per_page' => 1,
                'page_number'=> $pageNumber]
        ]);
        $data = collect();

        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function salesReturn($pageNumber=null)
    {
        $token = $this->getTokenFromSession();
        $shopId = session()->get('shopId');
        $endpoint = env('API_URL') . "/api/order/get-sales-return";

        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer " . $token
            ],
            'query' => ['shop_id'=>$shopId,
                'per_page' => 2,
                'page_number'=> $pageNumber]
        ]);
        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function salesReturnView($pageNumber=null,$id)
    {
        $token = $this->getTokenFromSession();

        $endpoint = env('API_URL') . "/api/order/get-sales-return/items";

        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer " . $token
            ],
            'query' => ['order_id'=>$id,
                'per_page' => 1,
                'page_number'=> $pageNumber]
        ]);
        $data = collect();

        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }
}
