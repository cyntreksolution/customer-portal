<?php

namespace App\Http\Controllers;

use App\Traits\ApiRequest;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    use ApiRequest;
    public function index(Request $request){
        $pageNumber = $request->pageId ?? 1 ;
        $orders = $this->sales($pageNumber);

        return view('sales.index',compact('orders'));
    }

    public function view(Request $request,$id){
        $pageNumber = $request->pageId ?? 1 ;
        $ordersItems = $this->salesView($pageNumber,$id);

        return view('sales.view',compact('ordersItems','id'));
    }
}
