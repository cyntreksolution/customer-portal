<?php

namespace App\Http\Controllers;

use App\Traits\ApiRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    use ApiRequest;


    public function index(){
        return view('login');
    }


    public function login(Request $request){

        $attr = $request->validate([
            'email' => 'required|string',
            'password' => 'required'
        ],
        [
            'email.required' => 'User name is required',
            'password.required' => 'Password is required'
        ]);


         $token = $this->getPersonalAccessToken($request->email, $request->password);

         if (isset($token) && !empty($token)) {
             return redirect(route('dashboard.index'));
         }

         Session::put('error','Credentials Mismatched..!');

         return redirect(route('login.index'));
    }

    public function logout(Request $request){
        $request->session()->forget('api_token');
        return redirect(route('login.index'));
    }
}
