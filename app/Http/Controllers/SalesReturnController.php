<?php

namespace App\Http\Controllers;

use App\Traits\ApiRequest;
use Illuminate\Http\Request;

class SalesReturnController extends Controller
{
    use ApiRequest;
    public function index(Request $request){
        $pageNumber = $request->pageId ?? 1 ;
        $salesReturns = $this->salesReturn($pageNumber);
        return view('sales-return.index',compact('salesReturns'));
    }

    public function view(Request $request,$id){
        $pageNumber = $request->pageId ?? 1 ;
        $returnItems = $this->salesReturnView($pageNumber,$id);

        return view('sales-return.view',compact('returnItems','id'));
    }
}
