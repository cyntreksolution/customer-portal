<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MembershipController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\SalesReturnController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [LoginController::class, 'index'])->name('login.index');
Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');


Route::group(['middleware' => ['token']], function () {

    Route::get('/dashboard', [DashBoardController::class, 'index'])->name('dashboard.index');

    Route::get('/membership/index', [MembershipController::class, 'index'])->name('membership.index');

    Route::get('/sales/index', [SalesController::class, 'index'])->name('sales.index');

    Route::get('/sales/view/{id}', [SalesController::class, 'view'])->name('sales.view');

    Route::get('/sales-return/index', [SalesReturnController::class, 'index'])->name('sales-return.index');

    Route::get('/sales-return/view/{id}', [SalesReturnController::class, 'view'])->name('sales-return.view');
});
